# ff_demo

### Summary

This demo will let you try out GitLab Feature Flags in a simple, easy to use scenario. Note that Feature Flags is a Premium/Silver feature, so you'll need a [paid](https://about.gitlab.com/pricing) GitLab account to try this out.

### Implementation

The API call configuration for `my_new_feature` exists in `main.go`, and the feature flag is toggleable at under the Feature Flags [settings](https://gitlab.com/jlenny/ff_demo/-/feature_flags) for this project.

`main.go` uses the Go Unleash [client library](https://github.com/Unleash/unleash-client-go) to update the feature flag status in real time - you don't need to restart the application to pick up the feature flag change.

### Instructions

#### One-Time Setup

1. Fork this project into your own project, where you'll be able to create and manage your own feature flags
1. Create a new feature flag under Operations->Feature Flags
1. Update the feature flag API information in `main.go` found under Operations->Feature Flags->Configure, being sure also to set the feature flag name to whatever you called your new feature flag; the API information is in the `init` function, and the feature flag name is set in the line `if unleash.IsEnabled("my_new_feature") {`

#### Demo

1. Run the `main.go` app
1. Visit `localhost:8080` to see the current feature flag behavior (it will say hello in all cases, but will add "check out my new feature!" if the feature flag is active)
1. Toggle the feature flag status in Operations->Feature Flags
1. Visit `localhost:8080` after a moment to see the feature flag status change

It's just that easy!

### More Information

Check out the complete documentation for this feature [here](https://docs.gitlab.com/ee/user/project/operations/feature_flags.html). If you have any questions, feel free to reach out to me at `jason at gitlab dot com` or on [Twitter](https://twitter.com/j4lenn).