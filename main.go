package main

import (
	"io"
	"log"
	"net/http"

	unleash "github.com/Unleash/unleash-client-go"
)

type metricsInterface struct {
}

func init() {
	unleash.Initialize(
		unleash.WithUrl("https://gitlab.com/api/v4/feature_flags/unleash/9192863"),
		unleash.WithInstanceId("XZ_Ldejx5YuhUK2Mgzzj"),
		unleash.WithAppName("production"),
		unleash.WithListener(&metricsInterface{}),
	)
}

func helloServer(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "Hello!\n")
	if unleash.IsEnabled("my_new_feature") {
		io.WriteString(w, "Check out my new feature!\n")
	}
}

func main() {
	http.HandleFunc("/", helloServer)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
